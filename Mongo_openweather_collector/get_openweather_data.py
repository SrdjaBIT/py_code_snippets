'''
Created on Dec 26, 2016

@author: sonic
'''

import os
import sys
import time
import pymongo
import datetime

data_collector_path = 'ow_API'
program = 'get_ow_data.go'
ow_data = 'go run %s/%s' % (data_collector_path, program)
start_mongo_cmd = 'systemctl start mongod'

alpine_mongo_cont = '172.17.0.2:27017'
host_machine_mongo = 'mongodb://localhost:27017/'

def start_mongodb():
    mongo_start_ret_val = os.popen(start_mongo_cmd).read()
    print(mongo_start_ret_val)

def get_data():
    current_openweather_data = os.popen(ow_data).read()
    data_list = current_openweather_data.splitlines()
    
    now = datetime.datetime.now()
    curr_time = now.strftime("%Y-%m-%d %H:%M:%S")
    
    # Connection to Mongo DB.
    try:
        client = pymongo.MongoClient(host_machine_mongo)
        print('Connected.')
    except(pymongo.errors.ConnectionFailure):
        print('Could not connect to MongoDB.')
        sys.exit()
    
    db = client['open_weather_data']
    result = db.open_weather_data.insert(
        {
            "date": curr_time,
            data_list[2]: {
                "id": data_list[0],
                "key": data_list[1],
                "country": data_list[3],
                "method": data_list[4],
                "weather": {
                    "temp": data_list[5],
                    "pressure": data_list[6],
                    "humidity": data_list[7],
                    "wind speed": data_list[8],
                    "wind deg": data_list[9]
                    },
                    "coord": [data_list[10], data_list[11]],
            }
        })
    print("Inserted object ID: %s" % result)


if __name__ == '__main__':
    start_mongodb()
    while(1):
        get_data()
        time.sleep(600)
        

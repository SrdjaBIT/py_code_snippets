package main

/*
	http://openweathermap.org/
	http://openweathermap.org/examples
	API KEY = 9342ddaa02330e39dc3ad262bd32effc
	https://github.com/briandowns/openweathermap
*/

import (
	"log"
	// Shortening the import reference name seems to make it a bit easier.
	f "fmt"
	owm "github.com/briandowns/openweathermap"
	"reflect"
)

type Collector struct {
	ID              int
	Key             string
	Name            string
	Country         string
	Base            string
	MainTemp        float64
	MainPressure    float64
	MainHumidity    int
	WindSpeed       float64
	WindDeg         float64
	GeoPosLatitude  float64
	GeoPosLongitude float64
	Cod             int
}


func get_openweather_data(city_country string) (Collector) {
	collection := Collector{}
	w, err := owm.NewCurrent("C", "EN")
	if err != nil {
		log.Fatalln(err)
	}

	w.CurrentByName(city_country)
	collection.ID = w.ID
	collection.Key = w.Key
	collection.Name = w.Name
	collection.Country = w.Sys.Country
	collection.Base = w.Base
	collection.MainTemp = w.Main.Temp
	collection.MainPressure = w.Main.Pressure
	collection.MainHumidity = w.Main.Humidity
	collection.WindSpeed = w.Wind.Speed
	collection.WindDeg = w.Wind.Deg
	collection.GeoPosLongitude = w.GeoPos.Longitude
	collection.GeoPosLatitude = w.GeoPos.Latitude
	collection.Cod = w.Cod

	return collection
}


func main() {
	ow_data_ns := get_openweather_data("Novi Sad, RS")
	ow_data_ny := get_openweather_data("New York, US")

	ow_data_ns_values := reflect.ValueOf(ow_data_ns)
	ns_values := make([]interface{}, ow_data_ns_values.NumField())

	for i := 0; i < ow_data_ns_values.NumField(); i++ {
		ns_values[i] = ow_data_ns_values.Field(i).Interface()
		f.Println(ns_values[i])
	}

	ow_data_ny_values := reflect.ValueOf(ow_data_ny)
	ny_values := make([]interface{}, ow_data_ny_values.NumField())

	for i := 0; i < ow_data_ny_values.NumField(); i++ {
		ny_values[i] = ow_data_ny_values.Field(i).Interface()
		f.Println(ny_values[i])
	}
}


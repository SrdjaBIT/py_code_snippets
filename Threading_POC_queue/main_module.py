'''
Created on Apr 1, 2016

@author: srdjan.furman
'''

import sys
from time import sleep
import queue
import threading
from asyncio.queues import QueueEmpty, QueueFull
import datetime


class_names = ['Class_8', 'Class_3']


class TestClass(object):

    def __init__(self, class_name, class_value):
        self._class_name = class_name
        self._val = class_value

    # Compute the "official" string representation of an object.
    def __repr__(self):
        return repr(self._class_name)


class UtilClass(object):

    def __init__(self, obj):
        self._obj = obj

    def compute(self):
        return self._obj._val


class PluginHandler(object):

    def __init__(self, t):
        self.t = t
        self.q = queue.Queue()
        self.p1 = Plugin_1()
        self.p2 = Plugin_2()

    def execute(self):
        print('start time: %s' % datetime.datetime.now().strftime('%H:%M:%S'))

        try:
            plugin_1 = threading.Thread(name='Method run() from Plugin_1',
                                        target=self.p1.run,
                                        args=(self.t, self.q))
            plugin_2 = threading.Thread(name='Method run() from Plugin_2',
                                        target=self.p2.run,
                                        args=(self.t, self.q))
            # Start threads concurrently.
            plugin_1.start()
            plugin_2.start()
        except Exception as e:
            print('%s' % e)

        # Wait until the last thread is finished using isAlive().
        # while plugin_1.isAlive() or plugin_2.isAlive():
        #     pass

        # Wait until the last thread is finished using join().
        # Block until all items in the queue have been gotten and processed.
        plugin_1.join()
        plugin_2.join()

        print('end time: %s' % datetime.datetime.now().strftime('%H:%M:%S'))
        res = 0
        # Try - catch not necessary here.
        while not self.q.empty():
            obj = self.q.get_nowait()
            print('Class from Q: {0}'.format(obj.__class__.__name__))
            name = '%s' % repr(obj)
            class_name = name.strip('"\'')
            # print('%s' % class_name)
            if class_name in class_names:
                print(class_name)
                util_class = UtilClass(obj)
                res += util_class.compute()
        print('Result = %s' % res)


class Plugin_1(object):

    def run(self, t, q):
        t += 1
        print('Function %s() from %s is doing it`s job '
              'for the next %s seconds.' %
              (self.run.__name__, self.__class__.__name__, t))
        sleep(t)

        for i in range(4):
            test_class = TestClass('Class_%s' % i, i)
            # Put an item into the queue - first check if it is full.
            try:
                q.put_nowait(test_class)
                # sleep(2)
            except QueueFull:
                raise Exception('Error - QueueFull')


class Plugin_2(object):

    def run(self, t, q):
        print('Function %s() from %s is doing it`s job '
              'for the next %s seconds.' %
              (self.run.__name__, self.__class__.__name__, t))
        sleep(t)

        for i in range(4, 9):
            test_class = TestClass('Class_%s' % i, i)
            # Put an item into the queue - first check if it is full.
            try:
                q.put_nowait(test_class)
                # sleep(1)
            except QueueFull:
                raise Exception('Error - QueueFull')


def sys_platform_version_info():

    print('%s.%s.%s' % (sys.version_info[0],
                        sys.version_info[1],
                        sys.version_info[2]))


if __name__ == '__main__':
    sys_platform_version_info()
    ph = PluginHandler(1)
    ph.execute()


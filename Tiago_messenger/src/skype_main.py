# -*- coding: utf-8 -*-

'''
Created on Jan 12, 2016

@author: sonic
'''

import Skype4Py
import wx
from textblob import TextBlob


class MyFrame(wx.Frame):

    def __init__(self, parent, apptitle, appsize):
        wx.Frame.__init__(self, parent, wx.ID_ANY, apptitle, size=appsize)

        panel = wx.Panel(self)

        # Explain send_button.
        self.send_button = wx.Button(panel, wx.ID_ANY, "Send")
        self.send_button.Bind(wx.EVT_BUTTON, self.on_flash)

        self.text_ctrl = wx.TextCtrl(panel, style=wx.TE_MULTILINE)
        display_txt = "Hi Tiago. How are you?"
        self.text_ctrl.AppendText(display_txt)

        # Create a horizontal_sizer space.
        horiz_sizer = wx.BoxSizer(wx.HORIZONTAL)
        horiz_sizer.Add(self.send_button, 0, wx.ALIGN_CENTER_VERTICAL)

        # Create a vertical_sizer space.
        vertical_sizer = wx.BoxSizer(wx.VERTICAL)
        vertical_sizer.Add(horiz_sizer, 0, wx.EXPAND | wx.ALL, 10)
        vertical_sizer.Add(self.text_ctrl, 1, wx.EXPAND | wx.ALL, 10)

        # Vertical sizer space contains horizontal sizer space, so set them.
        panel.SetSizer(vertical_sizer)

    def on_message_status(self, Message, Status):
        if Status == 'RECEIVED':
            en_blob = TextBlob(Message.Body)
            pt_blob = en_blob.translate(to="en")
            pt_return = str(pt_blob)

            pt_return_formatted = "\n" + \
                str(Message.FromDisplayName) + " : " + pt_return
            self.text_ctrl.AppendText(pt_return_formatted)

    def on_flash(self, evt):
        eng_message = self.text_ctrl.GetValue()

        en_blob = TextBlob(eng_message)
        skype.SendMessage("tiago.aloisio", en_blob.translate(to="pt"))
        skype.on_message_status = self.on_message_status


app = wx.App(0)
# Create a MyFrame instance and show the frame.
apptitle = "Tiago messenger"
width = 400
height = 200
MyFrame(None, apptitle, (width, height)).Show()

skype = Skype4Py.Skype()    # Create an instance of the Skype class.
skype.Attach()              # Connect the Skype object to the Skype client.

app.MainLoop()


import sys
import platform
import urllib.request, urllib.response
import re
import queue
import threading
from asyncio.queues import QueueEmpty, QueueFull


# Create a small application in Python that loads the data from
# https://pastebin.com/raw/WCLE2B4c, passes that data to another thread which
# parses the json and passes the result back to the original thread, which then
# extracts the value at the JSON path
# d4cd > fcf0 > f8f2 > 86fd > 395b > fd10 > f0ee and prints it.


def sys_platform_version_info():
    print('%s.%s.%s' % (sys.version_info[0],
                        sys.version_info[1],
                        sys.version_info[2]))
    print('%s, %s' % (platform.architecture()[0],
                      platform.architecture()[1]))


def load_data(url):
    return urllib.request.urlopen(url).read()


class ThreadStarter(object):
    def __init__(self, data):
        self.data = data
        self.q = queue.Queue()
        self.task_handler = TaskHandler()

    def do_task_in_thread(self):
        new_thread = None
        try:
            new_thread = threading.Thread(
                name='Method run() from TaskHandler',
                target=self.task_handler.get_result,
                args=(self.data, self.q))
            # Start thread.
            new_thread.start()
        except Exception as e:
            print('%s' % e)
        # Wait until the thread is finished using isAlive().
        while new_thread.isAlive():
            pass
        # Return and remove an item from the queue after finish.
        return self.q.get_nowait()


class TaskHandler(object):
    def __init__(self):
        pass

    def get_result(self, json_resp, q):
        # Convert a byte array into a JSON format.
        my_json = json_resp.decode('utf8').replace("'", '"')
        # Extract result from a json response.
        extract_result = my_json.split('{')[-1].split('}')[0]

        # Put an item into the queue - first check if it is full.
        try:
            q.put_nowait(extract_result)
        except QueueFull:
            raise Exception('Error - QueueFull')

        print("Method %s() from %s returns result." %
              (self.get_result.__name__, self.__class__.__name__))

        return extract_result


if __name__ == '__main__':
    location = 'https://pastebin.com/raw/WCLE2B4c'

    sys_platform_version_info()

    json_response = load_data(location)

    thread_starter = ThreadStarter(json_response)

    result = thread_starter.do_task_in_thread()

    print('Result: %s' % re.findall('"([^"]*)"', result)[1])
    
